﻿using AccountBalanceViwer.Data.Models;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccountBalanceViwer.Data.Data
{
    public class Seed
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public Seed(UserManager<User> userManager , RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// seed data into the database using the json file
        /// </summary>
        public void SeedUsers()
        {
            if(!_userManager.Users.Any())
            {
                var userData = System.IO.File.ReadAllText("F:\\Dev\\99x\\AccountBalanceViwer\\AccountBalanceViwer.Data\\Data\\UserSeedData.json");
                var users = JsonConvert.DeserializeObject<List<User>>(userData);

                var roles = new List<Role>
                {
                    new Role{Name = "NormalUser"},
                    new Role{Name = "Admin"}

                };

                foreach(var role in roles)
                {
                    _roleManager.CreateAsync(role).Wait();
                }

                foreach(var user in users)
                {
                    _userManager.CreateAsync(user, "123@Abc").Wait();
                    _userManager.AddToRoleAsync(user, "NormalUser").Wait();
                }

                var adminUser = new User
                {
                    UserName = "Admin"
                };

                IdentityResult result = _userManager.CreateAsync(adminUser, "123@Abc").Result;

                if (result.Succeeded)
                {
                    var admin = _userManager.FindByNameAsync("Admin").Result;
                    _userManager.AddToRolesAsync(admin, new[] { "Admin" }).Wait();
                }
            }
        }
    }
}
