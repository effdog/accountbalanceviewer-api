﻿using AccountBalanceViwer.Business.ManagerClasses.Interfaces;
using AccountBalanceViwer.Common;
using AccountBalanceViwer.Common.Dtos;
using AccountBalanceViwer.Data.Models;
using AccountBalanceViwer.Data.Repository.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AccountBalanceViwer.Business.ManagerClasses
{
    public class BalanceManager : IBalanceManager
    {
        #region Properties
        private readonly IBalanceRepository _balanceRepo;
        private readonly ILogger<BalanceManager> _logger;
        private readonly IConfiguration _config;
        #endregion

        #region Constructor
        public BalanceManager(IBalanceRepository balanceRepo, ILogger<BalanceManager> logger, IConfiguration config)
        {
            _balanceRepo = balanceRepo;
            _logger = logger;
            _config = config;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// To get the data from the excel template and save to database
        /// </summary>
        /// <param name="Data">Requires an object including the base64 string of the file and the file name</param>
        /// <returns>returns a filestatusDto object contaning status of the upload</returns>
        public async Task<FileStatusDto> ExcelUpload(ExcelFileData Data)
        {
            byte[] fileByte;
            FileStatusDto excelFileStatus = new FileStatusDto();

            //Decode base64 string
            try
            {
                string[] substrings = Data.FileContent.Split(',');
                string imgData = substrings[1];
                fileByte = Convert.FromBase64String(imgData);


                //Read excel using a file stream
                using (Stream fileStream = new MemoryStream(fileByte))
                {
                    using (var excelPackage = new ExcelPackage(fileStream))
                    {
                        var workSheet = excelPackage.Workbook.Worksheets[1];
                        int rowCount = workSheet.Dimension.Rows;
                        int colCount = workSheet.Dimension.Columns;

                        CellDataDto cellInfo = new CellDataDto();

                        //Starting from row two since the 1st row is headers
                        for (int row = 2; row <= rowCount; row++)
                        {

                            var year = workSheet.Cells[row, 1].Value;

                            //check if the year cell is has a value or a valid year format
                            if (year == null || string.IsNullOrEmpty(year.ToString().Trim()))
                            {
                                excelFileStatus.IsValid = false;
                                excelFileStatus.ErrorMessage = Constants.InvalidYear;
                                return excelFileStatus;
                            }
                            else if (year.ToString().Length != 4)
                            {
                                excelFileStatus.IsValid = false;
                                excelFileStatus.ErrorMessage = Constants.InvalidYear;
                                return excelFileStatus;
                            }
                            else
                            {
                                cellInfo.Year = year.ToString();
                            }

                            cellInfo.Month = workSheet.Cells[row, 2].Value.ToString();
                            cellInfo.RnD = workSheet.Cells[row, 3].Value.ToString();
                            cellInfo.Canteen = workSheet.Cells[row, 4].Value.ToString();
                            cellInfo.CeoCar = workSheet.Cells[row, 5].Value.ToString();
                            cellInfo.Marketing = workSheet.Cells[row, 6].Value.ToString();
                            cellInfo.ParkingFees = workSheet.Cells[row, 7].Value.ToString();

                        };

                        //create object for DB 
                        Balances valuesForDb = new Balances();
                        valuesForDb.Year = Convert.ToInt32(cellInfo.Year);
                        valuesForDb.Month = Convert.ToInt32(cellInfo.Month);
                        valuesForDb.RnD = Convert.ToDecimal(cellInfo.RnD);
                        valuesForDb.Canteen = Convert.ToDecimal(cellInfo.Canteen);
                        valuesForDb.CeoCar = Convert.ToDecimal(cellInfo.CeoCar);
                        valuesForDb.Marketing = Convert.ToDecimal(cellInfo.Marketing);
                        valuesForDb.ParkingFees = Convert.ToDecimal(cellInfo.Marketing);
                        valuesForDb.CreatedDateTime = DateTime.Now;
                        valuesForDb.UpdatedDateTime = DateTime.Now;

                        //check if records exists for the given year and month
                        bool isExists = await RecordsExistsInDb(valuesForDb.Year, valuesForDb.Month);

                        if (isExists)
                        {
                            excelFileStatus.IsValid = false;
                            excelFileStatus.ErrorMessage = Constants.ExcelDataAlreadyExists;

                            return excelFileStatus;
                        }

                        //insert recordsto DB    
                        bool isSuccess = await InsertFileDataToDb(valuesForDb);

                        if (isSuccess)
                        {
                            excelFileStatus.IsValid = true;

                            //uploading the file the azure blob storag
                            UploadTheExcelFileToBlob(Data.FileName, valuesForDb.Year, valuesForDb.Month, fileByte);
                        }
                        else
                        {
                            excelFileStatus.IsValid = false;
                            excelFileStatus.ErrorMessage = Constants.ErrorSavingToDb;
                        }
                    }
                    return excelFileStatus;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message.ToString());
                excelFileStatus.IsValid = false;
                excelFileStatus.ErrorMessage = Constants.ErrorUploadingExcel;
                return excelFileStatus;
            }

        }

        /// <summary>
        /// to upload the file into blob
        /// </summary>
        /// <param name="fileName">name of the file</param>
        /// <param name="year">year</param>
        /// <param name="month">month</param>
        /// <param name="file">file in a byte array</param>
        private void UploadTheExcelFileToBlob(string fileName, int year, int month, byte[] file)
        {
            string fileLocation = _config.GetSection("AppSettings:ExcelFolderPath").Value;
            string ContainerConnectionString = _config.GetSection("AppSettings:FileStorageConnection").Value;

            string[] stringArray = fileName.Split('.');
            string fileExtension = stringArray[stringArray.Count() - 1];

            string fileNameForBlob = "Template_" + year + "_" + month + "." + fileExtension;

            try
            {
                AccountBalanceViwer.Common.HelperMethods.UploadToBlob(file, fileNameForBlob, fileLocation, ContainerConnectionString);
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message.ToString());
            }
        }

        /// <summary>
        /// to check wethere uploaded excel contains matching data in the database
        /// </summary>
        /// <param name="year">int year</param>
        /// <param name="month">int month</param>
        /// <returns>returns a true or false</returns>
        public async Task<bool> RecordsExistsInDb(int year, int month)
        {
            bool isExists = false;

            if (await _balanceRepo.RecordsExitsts(year, month))
                isExists = true;

            return isExists;
        }

        /// <summary>
        /// Insert records to the database
        /// </summary>
        /// <returns>returns true or false</returns>
        public async Task<bool> InsertFileDataToDb(Balances valuesForDb)
        {
            bool isSuccess = false;
            try
            {
                if (await _balanceRepo.SaveBalanceData(valuesForDb))
                    isSuccess = true;

                _logger.LogInformation(Constants.DbSuccess);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message.ToString());
            }
            return isSuccess;
        }

        /// <summary>
        /// Get all data from balances tabel
        /// </summary>
        /// <returns></returns>
        public async Task<List<DataForTable>> GetBalance(int year)
        {
            List<DataForTable> newDataList = new List<DataForTable>();
            List<Balances> balance = await _balanceRepo.GetBalance(year);

            balance.ForEach(bal => newDataList.Add(
                new DataForTable()
                {
                    Canteen = bal.Canteen.ToString(),
                    CeoCar = bal.CeoCar.ToString(),
                    Marketing = bal.Marketing.ToString(),
                    ParkingFees = bal.ParkingFees.ToString(),
                    RnD = bal.RnD.ToString(),
                    Month = HelperMethods.GetMonthName(bal.Month),
                    Year = bal.Year.ToString()
                }));


            return newDataList;

        }


    } 
    #endregion
}
