﻿using AccountBalanceViwer.API.Controllers;
using AccountBalanceViwer.Business.ManagerClasses.Interfaces;
using AccountBalanceViwer.Common.Dtos;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AccountBalanceViewer.UnitTest
{
    public class BalanceControllerTests
    {
        private readonly Mock<IBalanceManager> _balanceManager = new Mock<IBalanceManager>();
        private readonly BalancesController _sut;

        public BalanceControllerTests()
        {
            _sut = new BalancesController(_balanceManager.Object);
        }

        
        [Fact]
        public async Task ExcelUpload_ShouldReturnBadRequestForNullData()
        {
            //Arrange 

            ExcelFileData fileData = new ExcelFileData
            {
                FileContent = null,
                FileName = null
            };

            //act
            var actual = await _sut.ExcelUpload(fileData);

            //asset
            actual.ToString().Equals(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ExcelUpload_ShouldReturnBadRequest_FileNameIsIncorrect()
        {
            //Arrange 

            ExcelFileData fileData = new ExcelFileData
            {
                FileContent = "RandomString",
                FileName = "Template_12.xlsx"
            };

            //act
            var actual = await _sut.ExcelUpload(fileData);

            //asset
            actual.ToString().Equals(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ExcelUpload_ShouldReturnOk()
        {
            //Arrange 

            ExcelFileData fileData = new ExcelFileData
            {
                FileContent = "RandomString",
                FileName = "Template.xlsx"
            };

            FileStatusDto returnDto = new FileStatusDto
            {
                IsValid = true
            };

            _balanceManager.Setup(x => x.ExcelUpload(fileData)).ReturnsAsync(returnDto);

            //act
            var actual = await _sut.ExcelUpload(fileData);

            //asset
            actual.ToString().Equals(HttpStatusCode.OK);
        }

        [Fact]
        public async Task ExcelUpload_ShouldReturn_BadRequest_IfIsValidisFalse()
        {
            //Arrange 

            ExcelFileData fileData = new ExcelFileData();

            FileStatusDto returnDto = new FileStatusDto
            {
                IsValid = false
            };

            _balanceManager.Setup(x => x.ExcelUpload(fileData)).ReturnsAsync(returnDto);

            //act
            var actual = await _sut.ExcelUpload(fileData);

            //asset
            actual.ToString().Equals(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task GetBalance_ShouldReturnOk()
        {
            //Arrange 

            int year = 2021;
            List<DataForTable> newList = new List<DataForTable>();

            _balanceManager.Setup(x => x.GetBalance(2021)).ReturnsAsync(newList);
            //act
            var actual = await _sut.GetBalance(year);

            //asset
            actual.ToString().Equals(HttpStatusCode.OK);
        }
    }
}
