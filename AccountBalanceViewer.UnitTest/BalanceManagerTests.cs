using AccountBalanceViwer.Business.ManagerClasses;
using AccountBalanceViwer.Data.Models;
using AccountBalanceViwer.Data.Repository.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace AccountBalanceViewer.UnitTest
{
    public class BalanceManagerTests
    {
        private readonly BalanceManager _sut;
        private readonly Mock<IBalanceRepository> _balanceRepoMock = new Mock<IBalanceRepository>();
        private readonly Mock<ILogger<BalanceManager>> _loggerMock = new Mock<ILogger<BalanceManager>>();
        private readonly Mock<IConfiguration> _configMock = new Mock<IConfiguration>();

        public BalanceManagerTests()
        {
            _sut = new BalanceManager(_balanceRepoMock.Object, _loggerMock.Object, _configMock.Object);
        }

        [Fact]
        public async Task GetBalance_ShouldReturnAListOfBalances()
        {
            //Arrange
            int year = 2021;
            int Month = 2;
            decimal RnD = 65;
            decimal Canteen = 44;
            decimal ceoCar = 96;
            decimal Marketing = 33;
            decimal ParkingFees = 44;


            List<Balances> newList = new List<Balances>()
            {
                new Balances {Id=1, Year=2021, Month=2, RnD=65, Canteen=44, CeoCar=96, Marketing=33, ParkingFees=44}
            };

            _balanceRepoMock.Setup(x => x.GetBalance(year)).ReturnsAsync(newList);

            //Act
            var resultBalance = await _sut.GetBalance(year);
           
            //Assert
            Assert.Equal(year, newList[0].Year);
            Assert.Equal(Month, newList[0].Month);
            Assert.Equal(RnD, newList[0].RnD);
            Assert.Equal(Canteen, newList[0].Canteen);
            Assert.Equal(ceoCar, newList[0].CeoCar);
            Assert.Equal(Marketing, newList[0].Marketing);
            Assert.Equal(ParkingFees, newList[0].ParkingFees);
        }


        [Fact]
        public async Task RecordsExistsInDb_ShouldReturnTrue()
        {
            //Arrange
            int year = 2021;
            int month =  2;

            _balanceRepoMock.Setup(x => x.RecordsExitsts(year, month)).ReturnsAsync(true);

            //Act
            var actual = await _sut.RecordsExistsInDb(year, month);

            //Assert
            Assert.True(actual);

        }

        [Fact]
        public async Task InsertFileDataToDb_ShouldReturnTrue()
        {
            //Arrange
            Balances balances = new Balances
            {
                Year = 2021, Month = 2, RnD = 21, Canteen = 23, CeoCar = 234, Marketing = 64 , ParkingFees = 23
            };  

            _balanceRepoMock.Setup(x => x.SaveBalanceData(balances)).ReturnsAsync(true);

            //Act
            var actual = await _sut.InsertFileDataToDb(balances);

            //Assert
            Assert.True(actual);
        }



    }
}
