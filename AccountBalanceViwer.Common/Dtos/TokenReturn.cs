﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountBalanceViwer.Common.Dtos
{
    public class TokenReturn
    {
        public string username { get; set; }
        public string token { get; set; }
    }
}
