﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountBalanceViwer.Common.Dtos
{

    public class FileStatusDto
    {
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }
}
