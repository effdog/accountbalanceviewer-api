﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountBalanceViwer.Common.Dtos
{
    public class ExcelFileData
    {
        public string FileContent { get; set; }
        public string FileName { get; set; }
    }

}
