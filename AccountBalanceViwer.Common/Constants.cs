﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountBalanceViwer.Common
{
    public static class Constants
    {
        public const string UserExists = "User Already Exists";

        public const string UserCreated = "User Created Successfully";

        public const string UserNotExists = "Username or Passwrod Incorrect";

        public const string FileName = "Template.xlsx";

        public const string IncorrectFile = "Incorrect Upload Template";

        public const string EmptyFile = "File is Empty";

        public const string ErrorUploadingExcel = "Error occured uploading excel";

        public const string ErrorSavingToDb = "Error when saving to database";

        public const string ExcelDataAlreadyExists = "Records already exists";

        public const string InvalidYear = "Invalid Year, Please correct the file and reupload";

        public const string DbSuccess = "Successfully Updated Database";

        public const string IncorretCreds = "Incorrect Credentials";

        public const string ErrorLogin  = "Login Error";
       
    }
}
